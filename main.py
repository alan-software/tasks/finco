from functools import reduce
from typing import Annotated
from fastapi import FastAPI, Query
import xmltodict
import requests
from locale import getlocale, getencoding, setlocale, LC_ALL, atof


app = FastAPI()
CB_URL = 'https://www.cbr.ru/scripts/XML_daily.asp'
rates_xml = requests.get(CB_URL).text
rates = xmltodict.parse(rates_xml, getencoding())
setlocale(LC_ALL, getlocale())

exchange_rates = reduce(lambda acc, item: acc | {item['CharCode']: atof(item['Value'])},
                        rates['ValCurs']['Valute'],
                        {'RUB': 1.0})

@app.get("/api/rates")
def convert_currency(from_: Annotated[str, Query(alias='from', pattern='^[A-Z]{3}$')],
                     to: Annotated[str, Query(pattern='^[A-Z]{3}$')],
                     value: Annotated[float, Query(ge=0)]):
    from_rate = exchange_rates[from_]
    to_rate = exchange_rates[to]
    result = value * from_rate / to_rate
    return {"result": result}
