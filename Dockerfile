FROM python:3

COPY requirements.txt /
COPY main.py /
RUN pip install -r requirements.txt
EXPOSE 8000
ENTRYPOINT ["uvicorn", "main:app", "--reload"]